# Miyoo Mini Daphne files

An archive for hosting (non-copyright) files relating to emulating Daphne arcade games on the Miyoo Mini (Onion OS).

In particular, I have created a miyoogamelist.xml for the Daphne system which points to the .zip rom files (not included), as well as images for each game which reside in Roms/DAPHNE/Imgs.

Secondly, I have created brand new framefiles for every game (even the non-working ones) which allows you to store each game in its own .daphe folder within the framefile folder, rather than the folder being a total mess of files.

# How to use

Copy all the contents of this repository onto the root of your SD card (aside from README.md). Inside the `Roms/DAPHNE/framefile` folder, you'll find a framefile .txt folder for each Daphne game. This file points to a folder `./<game>.daphne`, which will contain the video and audio files for each game.

This project is based heavily on the content found in an Internet Archive named "LaserDisc Games (Daphne & Mame)". You'll need to find this yourself.

## Example for Space Ace:

```
Roms
    |_ DAPHNE
       |_ framefile
       |  |_ ace.daphne
       |     |  |_ ace.commands
       |     |  |_ ace.dat
       |     |  |_ ace.m2v
       |     |  |_ ace.m2v.bf
       |     |  |_ ace.ogg
       |     |  |_ ace.ogg.bf
       |     |_ ace.txt
       |_ Imgs
          |_ ace.png
       |_ roms
          |_ ace.zip
       |_ pics
          |_ ...
       |_ ram
          |_ ...
       |_ sound
          |_ ...
```

# Daphne compatibility list for Miyoo Mini (Onion OS)

This is a general list of my observations of the games I've tested. This is my experience with my ROMset. If you find your games have different results, please let me know or, preferably, submit a merge request to update the table. If you manage to map controls for any "No control" game, please submit your remap file!

| Game                       | Rom file         | Status     | Notes                                                     | MD5                                                              |
|----------------------------|------------------|------------|-----------------------------------------------------------|------------------------------------------------------------------|
| Astron Belt                | astron.zip       | Good       |                                                           | B6F6F484F43CE799E780B50C26261B0DDDDD376A5A44111A291CEABD10567C71 |
| Badlands                   | badlands.zip     | Okay       | B to shoot. Crashes often                                 | 39D59088DF74F56AB7AF5C3BBFF674C94199641DC9B863FED32A84E19E124C3D |
| Bega's Battle              | bega.zip         | Good       |                                                           | F17A6B74A1216AA010F0A4640AAFC2B7BBC37DB24003B425D6DF492651F14569 |
| Cliff Hanger               | cliff.zip        | Unplayable | No control                                                | 908E26994C8A629093EB8E4D45FB59554FCE88690433702E5D2126CF210F35F2 |
| Cobra Command              | cobra.zip        | Good       |                                                           | 9982F583089851A11EC11C8EFC0D735B148D31498D2F53CD14E1E49617C5C75E |
| Dragon's Lair              | lair.zip         | Good       |                                                           | 24F1E307B4283512D4A054B1647608CEB384E845650C5BCC238E04BFA263AF05 |
| Dragon's Lair 2: Time Warp | lair2.zip        | Unplayable | No control                                                | 056B08994DD226C4A4CFF98294C818D381E82BEFF042682BB4D6AFA274EF359C |
| Esh's Aurunmilla           | esh.zip          | Unplayable | No control                                                | 440109EE1FC03EB104CB51496E29CDC2031636E12347BD83CAEAFD2DE79170AF |
| Galaxy Ranger              | galaxy.zip       | Unplayable | No control                                                | AAC837E413670796A17F9D6DEE4AD28435A18CF7697E8CA19239A05BAFB92450 |
| Interstellar Laser Fantasy | interstellar.zip | Unplayable | No control                                                | D409E23BC8078926FC08C1D164AAAA76BBF3F352FCFFDEAF95624E6619706BE7 |
| M.A.C.H. 3                 | mach3.zip        | Unplayable | No control                                                | 356F48F640C7E07A66D7213FC8DB2CA43737414923A6C6FB79A171C01B32A3B5 |
| Road Blaster               | roadblaster.zip  | Good       | Crashes often, requires launching several times initially | C84983AEF48D7D8D526D014F01040B782BE4773A96C47BBC9BDAAC766B3CA6A0 |
| Space Ace                  | ace.zip          | Good       | Press Y during energize sections and to use gun           | EDB39C65BE58C3C2B8EEAFD6F7D01804AB25E38FD87C011828E3CCDA7E27EC68 |
| Super Don Quix-ote         | sdq.zip          | Unplayable | No control                                                | 6BAC05EDB61B275DFDA0A5D057058FB2C40EA819EE7F0343BF2A1ECAFA25267C |
| Thayer's Quest             | tq.zip           | Unplayable | No control                                                | 0807524BFCBDF4A10E331AAE7AF7CD3820F8D867F0006E12BAA6A9CF4ECF1F87 |
| Us vs. Them                | uvt.zip          | No boot    | Freezes on boot screen                                    | 9D231BC163B94B3F0C6E8AC777863CF25FA196067B2C3B23249A8201EEE8A0E3 |
